#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#include "TimerOne.h"


#define OLED_MOSI   9 // SDA
#define OLED_SCK   10 // SCL
#define OLED_DC    11 // D/C
#define OLED_CS    12 // unused
#define OLED_RESET 13 // RST

#define SENSOR_PIN A4

#define DEBUG true


#define DELTA_LIGHT_OPEN 40 // feel free to play with this value. When measured value is DELTA_LIGHT + averageReading, then the shutter is open and we start measure for how long.
#define DELTA_LIGHT_CLOSE 10 // when we detect light smaller than average + light close, then the shutter is closed.


U8G2_SSD1306_128X64_NONAME_1_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ OLED_SCK, /* data=*/ OLED_MOSI, /* cs=*/ OLED_CS, /* dc=*/ OLED_DC, /* reset=*/ OLED_RESET);


int charW = 6;
int charH = 12;

int w = 128;
int h = 64;

long num = 0;

int sensorValue = 0;

long totalMeasuredValue = 0;
bool measuring = true;

bool lightStarted = false;

int averageReading = 0;

double shutter = 0;
double shutterSec = 0;


void callback() {
    num++;

    // measure for 2 seconds
    if (measuring && num > 8000) {
        measuring = false;

        averageReading = totalMeasuredValue/num;

        if (DEBUG) {
            Serial.print("Average reading: ");
            Serial.println(averageReading);

            Serial.println();
            Serial.println("LISTENING FOR LIGHT CHANGES!");
        }
    }

    if (num > 1000000) {
        num = 0;
    }

    // read 4000 times per second
    sensorValue = analogRead(SENSOR_PIN);

    // if we're measuring the light, add to the total value
    if (measuring) {
        totalMeasuredValue += sensorValue;
    } else {
        // else we check for big light differences and then mark the shutter open start
        if (!lightStarted && sensorValue > averageReading + DELTA_LIGHT_OPEN) {
            lightStarted = true;
            num = 0;

            shutter = 0;
            shutterSec = 0;

            if (DEBUG) {
                Serial.print("START:   sensorValue: ");
                Serial.print(sensorValue);
                Serial.println();
            }

        } else if (lightStarted && sensorValue < averageReading + DELTA_LIGHT_CLOSE) {

            lightStarted = false;

            if (num > 4000) {
                shutter = 0;
                shutterSec = ((double) num)/4000;
            } else {
                shutter = 4000 / (double) num;
                shutterSec = 0;
            }

            if (DEBUG) {
                Serial.print("NUM: ");
                Serial.print(num);
                Serial.print(", ");
                Serial.print("Shutter: ");
                Serial.print(shutter);
                Serial.print(", ");
                Serial.print("ShutterSec: ");
                Serial.println(shutterSec);

                Serial.print("END  :   sensorValue: ");
                Serial.print(sensorValue);
                Serial.println();
            }
        }
    }
}



int rowPx(int row) {
    return (row+1) * charH;
}

int rightPx(int len) {
    return 128 - (len * charW);
}


void rightTxt(int row, String txt) {  
    char __txt[txt.length()+1];
    txt.toCharArray(__txt, sizeof(__txt));

    u8g2.drawStr(rightPx(txt.length()), rowPx(row), __txt);
}

void leftTxt(int row, String txt) {
    char __txt[txt.length()+1];
    txt.toCharArray(__txt, sizeof(__txt));

    u8g2.drawStr(0, rowPx(row), __txt);
}

void drawStr(int x, int y, String txt) {
    char __txt[txt.length()+1];
    txt.toCharArray(__txt, sizeof(__txt));

    u8g2.drawStr(x, y, __txt);
}


void draw(void) {

    u8g2.setFont(u8g_font_6x12);

    if (measuring) {
        rightTxt(2, "*** calibration *** ");
    } else {
        rightTxt(0, "Ready");
    }

    if (shutterSec > 0) {

        rightTxt(2, String(shutterSec) + " sec");

    } else if (shutter > 0) {

        int shutterInt = round(shutter);

        String shutterSpeed = String(shutter);

        if (shutterInt == 1) {
            shutterSpeed = "1 sec";
        } else {
            shutterSpeed = "1/" + shutterSpeed;
        }
        
        rightTxt(2, shutterSpeed);
    } 

    if (lightStarted) {
        leftTxt(0, "*");
    }

}

void setup() {

    if (DEBUG) {
        Serial.begin(9600);
    }

    // init display
    u8g2.setColorIndex(1);

    // 1000 000 is a second
    // 1000 is 1/1000
    // 500 is 1/2000
    // 250 is 1/4000

    Timer1.initialize(250);
    Timer1.attachInterrupt(callback);

}


void loop() {

    u8g2.firstPage();
    do {
        draw();
    } while( u8g2.nextPage() );

    delay(500);
}

